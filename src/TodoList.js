import React from 'react';
import TodoItem from './TodoItem';

const TodoList = ({todo, handleCheckbox, deleteHandler, editHandler}) => {

    return (
        <div>

            {todo.map(item => {return ( 
                <TodoItem 
                    task={item.task} 
                    key={item.id} 
                    id={item.id} 
                    done={item.done}
                    handleCheckbox={handleCheckbox}
                    deleteHandler={deleteHandler}
                    editHandler={editHandler}
                />
            )})}
        
        </div>
    )

};

export default TodoList;