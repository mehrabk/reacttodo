import React from 'react';
import CustomToggleHook from './customHooks/toggleStateHook';
import EditForm from './EditForm';

const TodoItem = ({task, handleCheckbox, done, id, deleteHandler, editHandler}) => {

    const [isEdit, setEdit] = CustomToggleHook();

    return (
        <div style={{display: 'flex', alignItems: 'center'}}>
            
            <input type="checkbox" onClick={() => handleCheckbox(id)} value={done}/>
            
            {(isEdit) ? (
                <EditForm editHandler={editHandler} id={id} task={task} toggleEditForm={setEdit}/>
            ) : (
                <ul style={{textDecoration: done ? 'line-through' : 'none'}}>
                    <li>
                        {task}
                    </li>
                </ul>
            )}

            
            <div style={{margin: 20}}>    
                <button 
                    type="button"
                    onClick={setEdit}
                >
                    Edit
                </button>
                
                <button 
                    style={{marginLeft: 10}} 
                    type="button"
                    onClick={() => deleteHandler(id)}    
                >
                    Delete
                </button>
            </div>

        </div>
    )
}

export default TodoItem;