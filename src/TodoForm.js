import React from 'react';
import inputStateHook from './customHooks/inputStateHook';

const TodoForm = ({addTodo}) => {

    const [value, handleChange, resetInput] = inputStateHook();

    const submitHandler = (e) => {
        e.preventDefault();
        addTodo(value);
        resetInput();
    };

    return (
        
        <form onSubmit={submitHandler}>

            <input type="text" value={value} onChange={handleChange} />
            {/* <p>{value}</p> */}
        </form>

    )

};

export default TodoForm;