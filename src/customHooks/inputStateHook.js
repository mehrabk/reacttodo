//we will create custom hooks to handle input from user

import { useState } from 'react';

export default initialInput => {
    const [value, setValue] = useState('')
    const handleChange = (e) => {
        console.log("handleChange / value => ", value)
        setValue(e.target.value);
    }

    const resetInput = () => {
        setValue("");
    }

    return [value, handleChange, resetInput];
}