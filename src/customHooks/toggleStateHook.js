import { useState } from 'react';

export default function CustomToggleHook() {
    const [toggle, setToggle] = useState(false);

    const handleToggle = () => {
        setToggle(!toggle)
    };

    return [toggle, handleToggle];
}