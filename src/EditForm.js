import React from 'react';
import inputStateHook from './customHooks/inputStateHook';

const EditForm = ({editHandler, id, task, toggleEditForm}) => {

    const [value, handleChange, resetInput] = inputStateHook();

    console.log(value)
    const submitHandler = (e) => {
        e.preventDefault();
        editHandler(id, value);
        resetInput();
        toggleEditForm();
       
    }

    return (
        <form onSubmit={submitHandler}>
            <input type="text" placeholder={task} value={value} onChange={handleChange}/>
        </form>
    )
}

export default EditForm;