import { useState } from "react";

import TodoForm from './TodoForm';
import TodoList from './TodoList';

function App() {

  const dummyData = [{id: 1, task: 'write Code', done: false}];


  const [todo, setTodo] = useState(dummyData) 
  
  // add todo
  const addTodo = (newTask) => {
    const updateTodo = [
      ...todo,
      {
      id: Math.floor(Math.random() * 999),
      task: newTask,
      done: false  
      }
    ];

    setTodo(updateTodo);
  };

  const handleCheckbox = (id) => {
    const updateTodo = todo.map(item => 
      (item.id === id)
        ? {...item, done: !item.done}
        : item
      );

      setTodo(updateTodo);
  };

  const deleteHandler = (id) => {
    const updateTodo = todo.filter(item => item.id !== id);
    setTodo(updateTodo);
  };

  const editHandler = (id, newTask) => {
    const updateTodo = todo.map(item => 
      (item.id === id)
        ? {...item, task: newTask}
        : item
      );

      setTodo(updateTodo);
  }

  return (
    <div className="App">

      <TodoForm addTodo={addTodo} />

      <TodoList 
        todo={todo} 
        handleCheckbox={handleCheckbox}
        deleteHandler={deleteHandler}
        editHandler={editHandler}
      />

    </div>
  );
}

export default App;
